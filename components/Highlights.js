import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights(){
	return(
		<Row className="mt-3 mb-3">
                        <Col xs={12} md={4}>
                            <Card className="cardHighlight p-3">
                                <Card.Body>
                                    <Card.Title>
                                        <h2>Product Description</h2>
                                    </Card.Title>
                                    <Card.Text>
                                            Welcome to our digital illustration portrait shop, where we specialize in capturing the essence of individuals through stunning visual artistry. With our skilled team of talented artists and cutting-edge technology, we offer high-quality custom portraits that showcase the unique personality and beauty of each subject.Our Elegance Portrait Artwork is more than just a picture; it's a celebration of life, love, and memories. Crafted with precision and attention to detail, this custom-made masterpiece is designed to immortalize your most cherished moments.
                                    </Card.Text>
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col xs={12} md={4}>
                            <Card className="cardHighlight p-3">
                                <Card.Body>
                                    <Card.Title>
                                        <h2>Features</h2>
                                    </Card.Title>
                                    <Card.Text>
                                         From detailed pencil sketches to vibrant digital paintings, our diverse range of styles allows you to choose the perfect representation that resonates with your vision. Our seamless ordering process ensures a hassle-free experience, and you can easily submit your photos and preferences to receive a personalized masterpiece in no time. Whether it's a gift for a loved one or a cherished keepsake for yourself, our digital illustration portraits are the perfect way to immortalize special moments and create timeless art. Step into our shop and let us transform your memories into extraordinary works of digital art.                                    </Card.Text>
                                  </Card.Body>
                            </Card>
                        </Col>
                        <Col xs={12} md={4}>
                            <Card className="cardHighlight p-3">
                                <Card.Body>
                                    <Card.Title>
                                        <h2>How it Works</h2>
                                    </Card.Title>
                                    <Card.Text>
                                        Choose your preferred illustration style from our diverse selection.
                                        Upload your favorite photo for reference.
                                        Optionally, provide specific details or preferences for the artwork.
                                        Place your order securely through our user-friendly checkout process.
                                        Our talented artists will work on creating your custom digital illustration portrait.
                                        Once completed, we'll deliver the high-resolution digital artwork to you via email or download link.
                                        Admire and share your personalized masterpiece with friends and family.
                                        For any questions or assistance, our friendly customer support is here to help.
                                        Cherish your digital illustration portrait as a timeless keepsake or thoughtful gift.
                                        Let your memories and emotions come to life through our artistic expertise.
                                    </Card.Text>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
		)
}