import {Button, Row, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';
export default function Banner(){
	return(
		<Row>
		    <Col className="p-5">
		        <h1>Introducing Sketchy!</h1>
		        <p>A Timeless Masterpiece.</p>
		        <Button as = {Link} to = '/products' variant = "info">Shop now!</Button>
		    </Col>
		</Row>
	);
}